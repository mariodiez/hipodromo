#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include "interfazGrafica.h"
#include"CaballoIU.h"
#include"interfazUsuario.h"
void sistema(){

    system("title hipodromo");
    system("mode con cols=101 lines=22");
}

void inicInterfazUsuario()
{
    limpiapantalla1();
    gotoxy(1,1);
    printf("                                    H   I   P   O   D   R   O   M   O                                   ");
    int n;

    for (n=1; n<100; n++)
    {
        gotoxy(n,0);
        printf("%c",205);

    }

    for (n=1; n<100; n++)
    {
        gotoxy(n,2);
        printf("%c",205);
    }
    for (n=1; n<49; n++)
    {

        gotoxy(n,3);
        printf("%c",196);
    }
    for (n=52; n<100; n++)
    {

        gotoxy(n,3);
        printf("%c",196);
    }
    for (n=1; n<49; n++)
    {
        gotoxy(n,5);
        printf("%c",196);
    }
    for (n=52; n<100; n++)
    {
        gotoxy(n,5);
        printf("%c",196);
    }
    for (n=1; n<50; n++)
    {
        gotoxy(n,7);
        printf("%c",196);
    }
    for (n=52; n<100; n++)
    {
        gotoxy(n,7);
        printf("%c",196);
    }
    for (n=1; n<49; n++)
    {
        gotoxy(n,16);
        printf("%c",196);
    }
    for (n=52; n<100; n++)
    {
        gotoxy(n,16);
        printf("%c",196);
    }
    for (n=11; n<90; n++)
    {
        gotoxy(n,17);
        printf("%c",196);
    }
    for (n=11; n<90; n++)
    {
        gotoxy(n,19);
        printf("%c",196);
    }
    for (n=11; n<90; n++)
    {
        gotoxy(n,21);
        printf("%c",196);
    }
    for (n=1; n<2; n++)
    {
        gotoxy(0,n);
        printf("%c",186);
    }
    for (n=1; n<2; n++)
    {
        gotoxy(100,n);
        printf("%c",186);
    }
    for(n=4; n<5; n++)
    {
        gotoxy(0,n);
        printf("%c",179);
    }
    for (n=6; n<7; n++)
    {
        gotoxy(0,n);
        printf("%c",179);
    }
    for (n=4; n<5; n++)
    {
        gotoxy(100,n);
        printf("%c",179);
    }
    for (n=6; n<7; n++)
    {
        gotoxy(100,n);
        printf("%c",179);
    }
    for (n=8; n<16; n++)
    {
        gotoxy(0,n);
        printf("%c",179);
    }
    for (n=8; n<16; n++)
    {
        gotoxy(100,n);
        printf("%c",179);
    }
    for (n=18; n<19; n++)
    {
        gotoxy(10,n);
        printf("%c",179);
    }
    for (n=18; n<19; n++)
    {
        gotoxy(90,n);
        printf("%c",179);
    }
    for (n=20; n<21; n++)
    {
        gotoxy(10,n);
        printf("%c",179);
    }
    for (n=20; n<21; n++)
    {
        gotoxy(90,n);
        printf("%c",179);
    }
    for (n=4; n<16; n++)
    {
        gotoxy(49,n);
        printf("%c",179);
    }
    for (n=4; n<16; n++)
    {
        gotoxy(51,n);
        printf("%c",179);
    }

    gotoxy(0,0);
    printf("%c",201);

    gotoxy(100,0);
    printf("%c",187);

    gotoxy(100,2);
    printf("%c",188);

    gotoxy(0,2);
    printf("%c",200);

    gotoxy(0,3);
    printf("%c",218);

    gotoxy(100,3);
    printf("%c",191);

    gotoxy(0,5);
    printf("%c",195);

    gotoxy(100,5);
    printf("%c",180);

    gotoxy(0,7);
    printf("%c",195);

    gotoxy(100,7);
    printf("%c",180);

    gotoxy(0,16);
    printf("%c",192);

    gotoxy(100,16);
    printf("%c",217);

    gotoxy(49,3);
    printf("%c",191);

    gotoxy(51,3);
    printf("%c",218);

    gotoxy(49,5);
    printf("%c",180);

    gotoxy(51,5);
    printf("%c",195);

    gotoxy(49,7);
    printf("%c",180);

    gotoxy(51,7);
    printf("%c",195);

    gotoxy(49,16);
    printf("%c",217);

    gotoxy(51,16);
    printf("%c",192);

    gotoxy(10,17);
    printf("%c",218);

    gotoxy(90,17);
    printf("%c",191);

    gotoxy(10,19);
    printf("%c",195);

    gotoxy(90,19);
    printf("%c",180);

    gotoxy(10,21);
    printf("%c",192);

    gotoxy(90,21);
    printf("%c",217);

    return ;
}

int  menuPrincipal(){
    int opcion;
    gotoxy(18,4);
    printf("MENU PRINCIPAL");
    gotoxy(10,8);
    printf("%s","1.Gestion de caballos\n");
    gotoxy(10,9);
    printf("%s","2.Gestion de carreras\n");
    gotoxy(10,10);
    printf("%s","3.Inicio de carrera\n");
    gotoxy(10,11);
    printf("%s","4.Fin del programa\n");
    gotoxy(11,18);
    printf("%s","Elija una opcion:\n");
    gotoxy(28,18);
    scanf("%i",&opcion);

    limpiaRespuestas();
    limpiaMensajeInfo();
    return opcion;

}

void gestionMenuPrincipal(){
    int opcion,n;


    for(n=0; n>=0; n++)
    {
        opcion=menuPrincipal();

        switch(opcion)
        {

        case 1:
            muestraMensajeInfo("Usted ha elegido gestion del menu caballos\n");
            gestionMenuCaballos();
            break;
        case 2:
            limpiaMensajeInfo();
            muestraMensajeInfo("La funcion no esta implementada,seleccione otra opcion\n");
            opcion=menuPrincipal();
            limpiaRespuestas();
            break;
        case 3:
            muestraMensajeInfo("La funcion no esta implementada,seleccione otra opcion\n");
            opcion=menuPrincipal();
            limpiaRespuestas();
            break;
        case 4:
            gotoxy(11,20);
            muestraMensajeInfo("Usted ha elegido finalizar del programa \n");
            return;
            break;

        default:
            limpiaRespuestas();
            limpiaMensajeInfo();
            opcion=menuPrincipal();
            break;
        }
    }
    return;
}





