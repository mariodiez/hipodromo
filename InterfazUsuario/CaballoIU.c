#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <windows.h>
#include "interfazGrafica.h"
#include "CaballoIU.h"
#include"../InterfazSistema/CaballoSys.h"
#include"interfazUsuario.h"

void gestionMenuCaballos()
{
    int opcioncaballos, n;

    for(n=0; n<=10; n++)
    {

        opcioncaballos=menuCaballos();
        switch(opcioncaballos)
        {

        case 1:
            limpiapantalla1();
            inicInterfazUsuario();
            listadoCaballos("BaseDatos/caballos.txt");
            break;
        case 2:
            limpiapantalla1();
            inicInterfazUsuario();
            generaInformeCaballos();
            break;
        case 3:
            limpiapantalla1();
            inicInterfazUsuario();
            altaCaballoIU();
            break;
        case 4:
            limpiapantalla1();
            inicInterfazUsuario();
            actualizaCaballoIU();
            break;
        case 5:
            limpiapantalla1();
            inicInterfazUsuario();
            bajaCaballoIU();
            break;
        case 6:
            limpiapantalla1();
            inicInterfazUsuario();
            mayoresvictoriasIU();
            break;
        case 7:
            limpiapantalla1();
            inicInterfazUsuario();
            mayorRentabilidadIU();
            break;
        case 8:
            limpiapantalla1();
            inicInterfazUsuario();
            fusionDeCaballos();
            break;
        case 9:
            limpiapantalla1();
            inicInterfazUsuario();
            clasificaCaballos();
            break;
        case 10:
            limpiapantalla1();
            inicInterfazUsuario();
            fusionaListaCaballos();
            break;
        case 11:
            limpiapantalla1();
            inicInterfazUsuario();
            caballosPorPaises();
            break;
        case 12:
            limpiapantalla1();
            inicInterfazUsuario();
            return;
            break;
        default:
            gotoxy(28,18);
            scanf("%i",&opcioncaballos);
            limpiaRespuestas();
            limpiaMensajeInfo();
            opcioncaballos=menuCaballos();
        }
    }

}

int menuCaballos()
{
    limpiapantalla1();
    inicInterfazUsuario();
    limpiaRespuestas();
    int opcion;
    muestraMensajeInfo("Usted esta en el menu de gestion de caballos\n");

    gotoxy(18,4);
    printf("GESTION DE CABALLOS");
    gotoxy(10, 8);
    printf("1. Listado de caballos.");

    gotoxy(10, 9);
    printf("2. Informe de caballos.");

    gotoxy(10, 10);
    printf("3. Tramitar alta de un caballo.");

    gotoxy(10,11);
    printf("4. Actualizar datos de un caballo.");

    gotoxy(10,12);
    printf("5. Tramitar baja de un caballo.");

    gotoxy(10,13);
    printf("6. Buscar el caballo mas victorioso.");

    gotoxy(10,14);
    printf("7. Buscar el caballo mas rentable.");

    gotoxy(10,15);
    printf("8. Fusion de los caballos.");

    gotoxy(63,8);
    printf("9. Clasificacion de los caballos.");

    gotoxy(62,9);
    printf("10. Fusion de listas de caballos. ");

    gotoxy(62,10);
    printf("11. Caballos por nacionalidad. ");

    gotoxy(62,11);
    printf("12. Menu anterior.\n ");

    gotoxy(11,18);
    printf("%s","Elija una opcion:\n");

    gotoxy(30,18);
    scanf("%i",&opcion);
    limpiaMensajeInfo();
    limpiaRespuestas();
    return opcion;
}

void altaCaballoIU()
{
    gotoxy(18,4);
    printf("NUEVO CABALLO");
    int long die;
    int vic;
    float gan;
    char nombre[50],pais[50],msg[]="Ha habido un error,pulsa otro n�mero";
    gotoxy(11,18);
    printf("Usted esta dando de alta a un caballo\n");


    muestraMensajeInfo("Nombre: ");
    scanf("%s", &nombre[0]);
    gotoxy(10,8);
    printf("Nombre: %s",&nombre[0]);
    limpiaMensajeInfo();




    muestraMensajeInfo("DIE:");
    scanf("%ld", &die);
    gotoxy(10,9);
    printf("DIE:  %ld",die);
    limpiaMensajeInfo();



    muestraMensajeInfo("Ganancias:");
    scanf("%f", &gan);
    gotoxy(10,10);
    printf("Ganancias:  %9.3f", gan);
    limpiaMensajeInfo();




    muestraMensajeInfo("Victorias:");
    scanf("%d", &vic);
    gotoxy(10,11);
    printf("Victorias:  %d", vic);
    limpiaMensajeInfo();



    altaCaballoSys(pais,nombre,die,vic,gan,msg);
    gestionMenuCaballos();
    limpiapantalla1();
    inicInterfazUsuario();

    return ;
}

void listadoCaballos(char nombreArchivo[])
{
    int n,z,h=8,l=8;
    char msgFallo[200],pais[50][50], nombre[50][50],nombreArchivo0[50];
    long die[50];
    int vic[50];
    float gan[50];

    limpiapantalla1();
    inicInterfazUsuario();
    gotoxy(15,4);
    printf("LISTADO DE CABALLOS(1)");
    gotoxy(65,4);
    printf("LISTADO DE CABALLOS(2)");
    muestraMensajeInfo("Mostrando el listado de caballos\n");

    sprintf(nombreArchivo0,"BaseDatos/caballos.txt");



    gotoxy(5,6);
    printf("DIE:");
    gotoxy(41,6);
    printf("NOMBRE:");
    gotoxy(15,6);
    printf("VIC:");
    gotoxy(27,6);
    printf("GAN:");


    n=cargaListaCaballoSys(pais,nombre,die,vic,gan, msgFallo,nombreArchivo0);



    for(z=0; z<8; z++)
    {

        gotoxy(1,h);
        muestraCaballo(pais[z],die[z],vic[z],gan[z],nombre[z]);
        h++;

    }


    for(z=8; z<n; z++)
    {

        gotoxy(52,l);
        muestraCaballo(pais[z],die[z],vic[z],gan[z],nombre[z]);
        l++;

    }
    gotoxy(54,6);
    printf("DIE:");
    gotoxy(92,6);
    printf("NOMBRE:");
    gotoxy(66,6);
    printf("VIC:");
    gotoxy(77,6);
    printf("GAN:");

    getchar();
    getchar();
    return;
}

void actualizaCaballoIU()
{
    float gan[50],gan2;
    long die[50];
    bool p;
    int vic[50];
    int n,m;
    char nombre[50][50], nombre2[50],nombreArchivo0[50],pais[50][50], msgFallo[200]="Fallo al cargar la lista, pulse otro numero.";

    sprintf(nombreArchivo0,"BaseDatos/caballos.txt");

    listadoCaballos(nombreArchivo0);
    limpiaMensajeInfo();
    limpiaRespuestas();
    limpiaOpcion();

    muestraMensajeInfo("Escribe el nombre del caballo:");
    scanf("%s",nombre2);
    limpiaMensajeInfo();
    limpiaRespuestas();
    muestraMensajeInfo("Introduce los beneficios obtenidao:");
    scanf("%f",&gan2);


    n=cargaListaCaballoSys(pais,nombre,die,vic,gan,msgFallo,nombreArchivo0);

    if (n!=-1)
    {
        m=n;
        n=0;
        while(n<=m)
        {
            if(strcmp(nombre[n],nombre2)==0)
            {
                gan[n]=gan[n]+gan2;
                vic[n]=vic[n]+ 1;

            }
            n++;
        }
    }

    p=GuardaListaCaballoSys(n,pais,nombre, die, vic, gan,msgFallo,nombreArchivo0);
    if(p==TRUE)
    {
        muestraMensajeInfo("Se han actualizado correctamente los datos del caballo");
        listadoCaballos(nombreArchivo0);
        return;
    }


    else
    {
        limpiaMensajeInfo();
        limpiaRespuestas();
        muestraMensajeInfo("El animal seleccionado no se encuentra en la base de datos");
        getchar();
        return;
    }

}

void bajaCaballoIU()
{
    char nombre[50][50],nombreArchivo0[50],pais[50][50], msgFallo[]="FALLO EN LA OPERACION";
    long die[50], die2;
    bool found,p;
    float gan[50];
    int vic[50];
    int i,s,n;

    sprintf(nombreArchivo0,"BaseDatos/caballos.txt");

    listadoCaballos(nombreArchivo0);
    limpiaOpcion();
    limpiaMensajeInfo();
    limpiaRespuestas();


    muestraMensajeInfo("Introduce el DIE del caballo seleccionado:");
    scanf("%ld",&die2);

    n=cargaListaCaballoSys(pais,nombre,die,vic,gan,msgFallo,nombreArchivo0);

    for(i=0; i<=n; i++)
    {
        if (die[i]==die2)
        {
            found=true;
            for(s=i; s<n; s++)
            {
                die[s]=die[s+1];
                strcpy(nombre[s],nombre[s+1]);
                vic[s]=vic[s+1];
                gan[s]=gan[s+1];
            }

            break;
        }
    }
    if(found)
    {
        p=GuardaListaCaballoSys(n,pais, nombre, die, vic, gan,msgFallo,nombreArchivo0);
        if(p==FALSE)
        {
            muestraMensajeInfo(msgFallo);
            getchar();
            return;
        }
        else
        {
            muestraMensajeInfo("Se ha dado de baja correctamente");
            getchar();
            getchar();
            listadoCaballos(nombreArchivo0);
            getchar();
            return;
        }
    }
    return;
}

void mayoresvictoriasIU()
{

    int long die[50];
    float gan[50];
    char nombre[50][50],nombreArchivo0[50],pais[50][50],msgFallo[]="Ha habido un error al buscar el caballo con mayores victorias";
    int vic[50],i,n,k,ext;

    limpiapantalla1();
    inicInterfazUsuario();
    sprintf(nombreArchivo0,"BaseDatos/caballos.txt");
    n=cargaListaCaballoSys(pais,nombre,die,vic,gan,msgFallo,nombreArchivo0);
    k=0;
    ext=0;

    if(n!=-1)
    {
        for(i=0; i<n; i++)
        {
            if(vic[i]>ext)
            {
                ext=vic[i];
                k=i;
            }
        }
        limpiaMensajeInfo();
        gotoxy(5,6);
        printf("DIE:");
        gotoxy(40,6);
        printf("NOMBRE:");
        gotoxy(15,6);
        printf("VIC:");
        gotoxy(25,6);
        printf("GAN:");
        muestraMensajeInfo("El mas victorioso es:");
        gotoxy(35,20);
        printf("%s",nombre[k]);
        gotoxy(1,10);
        muestraCaballo(pais[k],die[k],vic[k],gan[k],nombre[k]);
        getchar();
        getchar();
        gestionMenuCaballos();
    }
    return;
}

void mayorRentabilidadIU()
{
    int i,n,k=0,vic[50];
    char nombre[50][50],pais[50][50],msgFallo[]="Ha habido un error al buscar el caballo mas rentable";
    float ext,gan[50];
    int long die[50];

    limpiapantalla1();
    inicInterfazUsuario();
    n=cargaListaCaballoSys(pais,nombre,die,vic,gan,msgFallo,"BaseDatos/caballos.txt");

    ext=0;

    if(n!=-1)
    {
        for(i=0; i<=n; i++)
        {
            if(gan[i]>ext)
            {
                ext=gan[i];
                k=i;
            }
        }
        limpiaMensajeInfo();
        gotoxy(5,6);
        printf("DIE:");
        gotoxy(40,6);
        printf("NOMBRE:");
        gotoxy(15,6);
        printf("VIC:");
        gotoxy(25,6);
        printf("GAN:");
        gotoxy(3,8);
        muestraMensajeInfo("El que ofrece mayor rentabilidad es:");
        gotoxy(1,10);
        muestraCaballo(pais[k],die[k],vic[k],gan[k],nombre[k]);
        gotoxy(50,20);
        printf("%s",nombre[k]);
        getchar();
        getchar();
        gestionMenuCaballos();
        return;
    }
    return;
}

void muestraListaCaballos(int n,char pais[][50],char nombre[][50],long die[],int vic[],float gan[],char nombreArchivo[])
{
    int z,h=8,l=8;
    char  msgFallo[200]="Fallo al cargar la lista, pulse otro numero.";


    limpiapantalla1();
    inicInterfazUsuario();
    gotoxy(15,4);
    printf("LISTADO DE CABALLOS(1)");
    gotoxy(65,4);
    printf("LISTADO DE CABALLOS(2)");

    n=cargaListaCaballoSys(pais,nombre,die,vic,gan,msgFallo,nombreArchivo);


    gotoxy(5,6);
    printf("DIE:");
    gotoxy(41,6);
    printf("NOMBRE:");
    gotoxy(15,6);
    printf("VIC:");
    gotoxy(27,6);
    printf("GAN:");


    for(z=0; z<8; z++)
    {
        if(z>=n)
        {
            break;
        }
        gotoxy(1,h);
        muestraCaballo(pais[z],die[z],vic[z],gan[z],nombre[z]);
        h++;

    }


    for(z=8; z<n; z++)
    {

        gotoxy(52,l);
        muestraCaballo(pais[z],die[z],vic[z],gan[z],nombre[z]);
        l++;

    }
    gotoxy(54,6);
    printf("DIE:");
    gotoxy(92,6);
    printf("NOMBRE:");
    gotoxy(66,6);
    printf("VIC:");
    gotoxy(77,6);
    printf("GAN:");
    return;
}

void clasificaCaballos()
{
    int vic[50],vic2[50],victotal,n3,m=0,z;
    char nombre[50][50],nombre2[50][50],pais[50][50],msgFallo[200]="Se ha producido un error",nombreArchivo0[50],nombreArchivo4[50];
    float gan[50],gan2[50];
    long die[50],die2[50];

    sprintf(nombreArchivo0,"BaseDatos/caballos.txt");
    sprintf(nombreArchivo4,"BaseDatos/caballos4.txt");

    muestraMensajeInfo("Introduzca el numero de victorias para la clasificacion:");
    scanf("%d",&victotal);

    n3=cargaListaCaballoSys(pais,nombre,die,vic,gan,msgFallo,nombreArchivo0);


    if(n3==-1)
    {
        muestraMensajeInfo(msgFallo);
    }
    else
    {


        for(z=0; z<n3; z++)
        {
            if(victotal<=vic[z])
            {

                strcpy(nombre2[m],nombre[z]);
                die2[m]=die[z];
                vic2[m]=vic[z];
                gan2[m]=gan[z];
                m++;
            }
        }

        GuardaListaCaballoSys(m+1,pais,nombre2,die2,vic2,gan2,msgFallo,nombreArchivo4);
        listadoCaballos(nombreArchivo4);
    }
}

void fusionDeCaballos()
{
    char nombre[50][50],nombre2[50][50],nombreArchivo1[50],nombreArchivo2[50],nombreArchivo3[50],pais[50][50],msgFallo[200]="ERROR";
    long die[50],die2[50];
    int vic[50],vic2[50],n1,n2,j,m;
    float gan[50],gan2[50];

    sprintf(nombreArchivo1,"BaseDatos/caballos1.txt");
    sprintf(nombreArchivo2,"BaseDatos/caballos2.txt");
    sprintf(nombreArchivo3,"BaseDatos/caballos3.txt");

    n1=cargaListaCaballoSys(pais,nombre,die,vic,gan,msgFallo,nombreArchivo1);
    n2=cargaListaCaballoSys(pais,nombre2,die2,vic2,gan2,msgFallo,nombreArchivo2);


    for(j=0; j<n2; j++)
    {
        for(m=0; m<n1; m++)
        {
            if(strcmp(nombre[m],nombre2[j])==0)
            {
                strcpy(nombre[m], nombre2[j]);
                die[m]=die2[j];
                gan[m]=gan[m]+gan2[j];
                vic[m]=vic[m]+vic2[j];
                break;
            }
        }
        if (strcmp(nombre[m],nombre2[j])!=0)
        {
            strcpy(nombre[n1], nombre2[j]);
            gan[n1]=gan2[j];
            vic[n1]=vic2[j];
            die[n1]=die2[j];
            n1++;
        }
    }
    GuardaListaCaballoSys(n1,pais,nombre,die,vic,gan,msgFallo,nombreArchivo3);
    muestraListaCaballos(n1,pais,nombre,die,vic,gan,nombreArchivo3);
    muestraMensajeInfo("Mostrando el listado fusionado de los caballos");

    getchar();
    getchar();
}

void fusionaListaCaballos()
{
    char nombre[50][50],nombre2[50][50],nombre3[50][50],nombreArchivo1[50],nombreArchivo2[50],nombreArchivo5[50],pais[50][50],msgFallo[200]="ERROR";
    long die[50],die2[50],die3[50];
    int vic[50],vic2[50],vic3[50],n1,n2,j,m,s=0;
    float gan[50],gan2[50],gan3[50];

    sprintf(nombreArchivo1,"BaseDatos/caballos1.txt");
    sprintf(nombreArchivo2,"BaseDatos/caballos2.txt");
    sprintf(nombreArchivo5,"BaseDatos/caballos5.txt");


    n1=cargaListaCaballoSys(pais,nombre,die,vic,gan,msgFallo,nombreArchivo1);
    n2=cargaListaCaballoSys(pais,nombre2,die2,vic2,gan2,msgFallo,nombreArchivo2);
    if(n1==-1 || n2==-1)
    {

        muestraMensajeInfo(msgFallo);
    }
    else
    {
        muestraMensajeInfo("Mostrando primera lista de caballos");
        muestraListaCaballos(n1,pais,nombre,die,vic,gan,nombreArchivo1);
        muestraMensajeInfo("Pulse la tecla ENTER para pasar a la siguiente lista");
        getchar();
        getchar();
        limpiapantalla1();
        inicInterfazUsuario();
        muestraMensajeInfo("Mostrando segunda lista de caballos");
        muestraListaCaballos(n2,pais,nombre2,die2,vic2,gan2,nombreArchivo2);
        muestraMensajeInfo("Pulse la tecla ENTER para pasar a la lista final ");
        getchar();



        for(j=0; j<n2; j++)
        {
            for(m=0; m<n1; m++)
            {
                if(die2[j]==die[m])
                {
                    strcpy(nombre3[s],nombre2[j]);
                    die3[s]=die2[j];
                    vic3[s]=(vic2[j]+vic[m]);
                    gan3[s]=(gan2[j]+gan[m]);
                    s++;
                    break;
                }
            }
        }

        GuardaListaCaballoSys(s+1,pais,nombre3,die3,vic3,gan,msgFallo,nombreArchivo5);
        muestraListaCaballos(s+1,pais,nombre3,die3,vic3,gan3,nombreArchivo5);
        muestraMensajeInfo("Mostrando la lista final de caballos fusionados");
        getchar();


    }

    return;
}

void caballosPorPaises()
{

    int vic5[50],vic4[50],n4,m=0,z;
    char nombre5[50][50],nombre4[50][50],pais[50][50],pais2[50][50],paistotal[50],msgFallo[200]="Se ha producido un error",nombreArchivo6[50],nombreArchivo7[50];;
    float gan5[50],gan4[50];
    long die5[50],die4[50];

    sprintf(nombreArchivo6,"BaseDatos/caballosplus.txt");


    n4=cargaListaCaballoSys(pais,nombre4,die4,vic4,gan4,msgFallo,nombreArchivo6);
    muestraListaCaballos(n4,pais,nombre4,die4,vic4,gan4,nombreArchivo6);

    muestraMensajeInfo("Introduzca las siglas de la nacionalidad a clasificar:");
    scanf("%s",paistotal);
    sprintf(nombreArchivo7,"BaseDatos/%s.txt",paistotal);

    //nombreArchivo7[50]=paistotal[50];


    if(n4==-1)
    {
        muestraMensajeInfo(msgFallo);
    }
    else
    {

        for(z=0; z<n4; z++)
        {
            if(strcmp(paistotal,pais[z])==0)
            {

                strcpy(pais2[m],pais[z]);
                die5[m]=die4[z];
                vic5[m]=vic4[z];
                gan5[m]=gan4[z];
                strcpy(nombre5[m],nombre4[z]);
                m++;
            }
        }
        GuardaListaCaballoSys(m+1,pais2,nombre5,die5,vic5,gan5,msgFallo,nombreArchivo7);
        muestraListaCaballos(m+1,pais2,nombre5,die5,vic5,gan5,nombreArchivo7);
        muestraMensajeInfo("Mostrando los caballos por nacionalidad");
    }
    getchar();
    getchar();

}



































