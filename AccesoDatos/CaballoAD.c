#include <windows.h>
#include <stdbool.h>
#include<stdio.h>
#include"CaballoAD.h"
#include"../InterfazUsuario/interfazGrafica.h"
#include"../InterfazUsuario/interfazUsuario.h"

int cargaListaCaballoAD(char pais[][50],char nombre[][50], long die[],int vic[],float gan[],char msgFallo[],char nombreArchivo[])
{
    int n;
    FILE*ptr;

    ptr=fopen (nombreArchivo, "rt" );
    if(ptr==NULL)
    {
        return -1;
    }
    n=0;
    while(fscanf(ptr,"%s",pais[n])==1)
    {

        fscanf(ptr,"%ld", &die[n]);
        fscanf(ptr,"%d",&vic[n]);
        fscanf(ptr,"%f",&gan[n]);
        fscanf(ptr,"%s",nombre[n]);

        n++;
    }
    fclose(ptr);

    return n;
}
void escribeCaballoTXT(char pais[],int long die, int vic,float gan,char nombre[],FILE *ptr)
{
    fprintf(ptr,"\n %s\t\t %ld\t\t %d\t\t %9.3f\t  %s",pais,die,vic,gan,nombre);
    return;
}
bool altaCaballoAD (char pais[50],char nombre[50], long die, int vic, float gan,char msg[100])
{
    FILE*ptr;

    ptr=fopen("BaseDatos/caballos.txt","at");

    if(ptr==NULL)
    {
        muestraMensajeInfo(msg,"No se puede abrir el archivo caballos.txt");
        return false;
    }
    fprintf(ptr,"\n%5s %5ld %6d %11.2f %14s\n",pais,die, vic, gan,nombre);
    fclose(ptr);
    return true;
}

bool GuardaListaCaballoAD(int n, char pais[][50],char nombre[][50], long die[], int vic[], float gan[],char nombreArchivo[]){
        int k;

        FILE *ptr;
        ptr=fopen(nombreArchivo,"wt");

        if(ptr==NULL){
            muestraMensajeInfo("Se ha producido un error en la actualizacion del caballo");
            return false;
        }
        k=n-1;
        for(n=0;n<k;++n){
            escribeCaballoTXT(pais[n],die[n],vic[n],gan[n],nombre[n],ptr);
        }
        fclose(ptr);
        return true;
        }








