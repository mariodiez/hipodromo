#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <stdbool.h>
#include"../InterfazSistema/CaballoSys.h"
#include"../InterfazUsuario/interfazGrafica.h"
#include"../AccesoDatos/CaballoAD.h"
#include"../InterfazUsuario/interfazUsuario.h"

int cargaListaCaballoSys(char pais[][50],char nombre[][50], long die[], int vic[],float gan[], char msgFallo[],char nombreArchivo[])
{
    int n;
    char c[150];
    n=cargaListaCaballoAD(pais,nombre,die,vic,gan,msgFallo,nombreArchivo);

    if (n==-1)
    {

        muestraMensajeInfo(msgFallo);
        scanf("%s",c);
        return -1;
    }

    return n;
}

void muestraCaballo(char pais[50],long die,int vic, float gan,char nombre[50])
{
    printf("%5s %7ld %8d %13.2f %14s\n",pais,die,vic,gan,nombre);


    return;

}

bool altaCaballoSys (char pais[50],char nombre[], long die, int victorias, float ganancias,char msg[])
{

    bool valor;
    valor=altaCaballoAD (pais, nombre, die, victorias, ganancias, msg);
    if (valor)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool generaInformeCaballos()
{
    char nombre[50],c[50],pais[50];
    int long die;
    int vic;
    float gan;

    FILE *ptr,*ptr1;

    ptr=fopen("BaseDatos/caballosplus.txt","rt");
    ptr1=fopen("BaseDatos/infocaballos.txt","wt");

    if(ptr==NULL || ptr1==NULL)
    {
     muestraMensajeInfo("Error al crear el informe, pulse otra tecla para avanzar");
     scanf("%s",c);
     return false;
    }
    fprintf(ptr1,"-------------------------------------------------------------------------");
    fprintf(ptr1,"\n PAIS\t \tDIE\t \tVICTORIAS\t GANANCIAS\t NOMBRE");
    fprintf(ptr1,"\n-----------------------------------------------------------------------");
    limpiapantalla1();
    limpiaMensajeInfo();
    inicInterfazUsuario();

     while(fscanf(ptr,"%s %ld %d %f %s ",pais,&die,&vic,&gan,nombre)!=EOF){
        escribeCaballoTXT(pais,die,vic,gan,nombre,ptr1);
     }

    fclose(ptr);
    fclose(ptr1);

    system("notepad BaseDatos/infocaballos.txt");

    muestraMensajeInfo("Pulse la tecla ENTER para volver al menu");
    getchar();
    getchar();

    gestionMenuCaballos();
    limpiapantalla1();
    inicInterfazUsuario();
    return true;
}

bool GuardaListaCaballoSys(int n,char pais[][50], char nombre[][50], long die[], int vic[], float gan[],char msgFallo[],char nombreArchivo[]){
    bool p;
    msgFallo="Se ha producido un error";

    p=GuardaListaCaballoAD(n,pais,nombre, die, vic, gan, nombreArchivo);
     return true;
    if(p==FALSE){
        muestraMensajeInfo(msgFallo);
        getchar();
        return false;
    }

}
